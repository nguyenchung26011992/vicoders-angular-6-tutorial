import { Component } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  form: FormGroup;

  firstName = new FormControl("", Validators.required);

  constructor(fb: FormBuilder) {
    this.form = fb.group({
      firstName: this.firstName,
      password: ["", Validators.required]
    });
  }

  onSubmit() {
    console.log("model-based form submitted");
    console.log(this.form);
  }

  partialUpdate() {
    this.form.patchValue({ firstName: "Partial" });
  }

  fullUpdate() {
    this.form.setValue({ firstName: "Partial", password: "monkey" });
  }

  reset() {
    this.form.reset();
  }
}
