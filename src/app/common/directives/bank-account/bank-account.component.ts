import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-bank-account',
  templateUrl: './bank-account.component.html',
  styleUrls: ['./bank-account.component.scss']
})
export class BankAccountComponent implements OnInit {
  @Input('account_id') account_id;
  @Input('bank_name') bank_name;

  @Output() parentAccountIdChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
    
  }

  onAccountIdChange(account_id) {
    this.parentAccountIdChange.emit(account_id);
  }
}
